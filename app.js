const can = require('socketcan');
const async = require("async");
const exec = require('child_process').exec;

const gpio = require("@tibbo-tps/gpio");

var button, blink, led1, led2, db0, db1, i = 0, j = 1;
var wasButtonPressed = false;

const initGpio = function(){
    try{
        button = gpio.init("S1A");
        button.setDirection("input");

        led1 = gpio.init("S13A");
        led1.setDirection("output");

        led1.setValue(1);

        led2 = gpio.init("S15A");
        led2.setDirection("output");

        led2.setValue(1);
    }catch(e){

    }
};

const doExec = function(command){
    return function(callback){
        exec(command, (err, stdout, stderr) => {
            callback(err)
        })
    }
};

const reset = function(callback){
    async.series([
        doExec('ip link set dev can0 down'),
        doExec('ip link set dev can1 down'),
        doExec('ip link set dev can0 up type can bitrate 125000'),
        doExec('ip link set dev can1 up type can bitrate 125000')
    ], callback);
};

const init = function(){
    return new Promise((resolve, reject) => {
        reset((err) => {
            if(err === null){
                try{
                    var network = can.parseNetworkDescription("./kcd/sample_device.kcd");

                    var channel0 = can.createRawChannel("can0");
                    var channel1 = can.createRawChannel("can1");

                    db0 = new can.DatabaseService(channel0, network.buses["Tank"]);
                    db1 = new can.DatabaseService(channel1, network.buses["Tank"]);

                    channel0.start();
                    channel1.start();

                    db0.messages["TankController"].signals["LiquidTemp"].onChange((signal) => {
                        //console.log(signal.value);
                        try{
                            led1.setValue(signal.value);
                        }catch(e){
                            console.log(e);
                        }
                    });

                    db1.messages["TankController"].signals["HeaterTemp"].onChange((signal) => {
                        //console.log(signal.value);
                        try{
                            led2.setValue(signal.value);
                        }catch(e){
                            console.log(e);
                        }
                    });

                    resolve()
                }catch(err){
                    reject();
                }
            }else{
                reject();
            }
        });
    })
};

const errState = function(){
    var i = 0;
    setInterval(() => {
        led1.setValue(i);
        led2.setValue(i);

        i = i === 0 ? 1 : 0;
    }, 500)
};

const scan = function(){
    setInterval(function(){
        // If button is just released...
        if(button.getValue() === 1 && wasButtonPressed === true){
            try{
                clearInterval(blink)
            }catch(e){

            }

            wasButtonPressed = false;

            reset(() => {
                led1.setValue(1);
                led2.setValue(1);

                console.log("released");
            })
        }else if(button.getValue() === 0 && wasButtonPressed === false){
            // If button is pressed

            wasButtonPressed = true;

            blink = setInterval(() => {
                db0.messages["TankController"].signals["HeaterTemp"].update(i);
                i = i === 0 ? 1 : 0;
                db0.send("TankController");

                db1.messages["TankController"].signals["LiquidTemp"].update(j);
                j = j === 0 ? 1 : 0;
                db1.send("TankController");
            }, 500);

            console.log("pressed");
        }
    },100);
};

initGpio();

init()
    .then(() => {
        console.log("initialized");
        scan();
    })
    .catch((e) => {
        console.log("unable to init");
        errState();
    });

process.on("SIGINT", () => {
    async.series([
        doExec('ip link set dev can0 down'),
        doExec('ip link set dev can1 down')
    ], () => {
        led1.setValue(1);
        led2.setValue(1);
        process.exit();
    })
});

/*db1.messages.forEach((message) => {
    var signals = message.signals;
    Object.keys(signals)
        .forEach((signalName) => {
            var signal = signals[signalName];
            signal.onChange((signal) => {
                console.log(signal.name + " changed to "+signal.value)
            })
        })
});*/

/*
var i = 0, j = 1;

setInterval(() => {
    db0.messages["TankController"].signals["LiquidTemp"].update(i);
    i = i === 0 ? 1 : 0;
    db0.send("TankController");

    db1.messages["TankController"].signals["LiquidTemp"].update(j);
    j = j === 0 ? 1 : 0;
    db1.send("TankController");
}, 500);

setTimeout(() => {
    channel0.stop();
    channel1.stop();
}, 10000);

process.on('exit', () => {
    channel0.stop();
    channel1.stop();
});*/
